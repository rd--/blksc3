blksc3
------

Block SuperCollider,
or SuperBlock,
is a [Block](https://developers.google.com/blockly) editor for the
[SuperCollider](https://www.audiosynth.com/) synthesiser.

![](https://rohandrape.net/sw/blksc3/svg/BlockSuperCollider.svg)

<!-- <img src="https://rohandrape.net/sw/blksc3/png/WhyBlockSuperCollider.2.png" width="568" height="338"> -->

Texts:

- [Extended Abstract](https://rohandrape.net/?t=blksc3&e=md/extended-abstract.md)
- [Glossary](https://rohandrape.net/?t=blksc3&e=md/glossary.md)
- [References](https://rohandrape.net/?t=blksc3&e=md/references.md)

Online editor:
[blksc3.rohandrape.net](https://blksc3.rohandrape.net/)

Requires:
[jssc3](https://rohandrape.net/?t=jssc3),
[spl](https://rohandrape.net/?t=spl)

Tested with:

[Chromium](https://www.chromium.org/) 131.0.6778.85,
[Chrome](https://www.google.com/chrome/) 98.0.4758.102,
[Firefox](https://www.mozilla.org/firefox/) 91.11.0 & 97.0,
[Edge](https://www.microsoft.com/edge) 97.0.1072.62,
[Safari](https://apple.com/safari/) 15.3,
[SuperCollider](https://www.audiosynth.com/) 3.13.0,
[Blockly](https://developers.google.com/blockly) 11.2.0,
[Commonmark.js](https://github.com/commonmark/commonmark.js/) 0.30.0

© [Rohan Drape](http://rohandrape.net/), 2021-2025, [Gpl](http://gnu.org/copyleft/)

* * *

[Html](https://blksc3.rohandrape.net)
[Video](https://rohandrape.net/?t=blksc3&e=md/video.md)
[Cli](https://rohandrape.net/?t=blksc3&e=md/blksct3.md)
[Blockly](https://developers.google.com/blockly)
