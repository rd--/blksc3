# References

Assayag, G., and C. Agon. 1996. “OpenMusic Architecture.” In *Proc.
ICMC*. <https://hdl.handle.net/2027/spo.bbp2372.1996.103>.

Begel, A. 1996. “LogoBlocks: A Graphical Programming Language for
Interacting with the World.” AUP Thesis, Cambridge, MA: MIT Media Lab.

Berners-Lee, T. J., R. Cailliau, and J-F. Groff. 1992. “The World-Wide
Web.” *Computer Networks and ISDN Systems* 25 (4): 454–59.
<https://doi.org/10.1016/0169-7552(92)90039-S>.

Borning, A. H. 1977. “ThingLab: An Object-Oriented System for Building
Simulations Using Constraints.” In *Proc. International Joint Conference
on Artificial Intelligence*, 497–98.

Bricklin, D. 1979. *VisiCalc: A Visible Calculator for the Apple II,
Reference Card*. Sunnyvale, CA: Software Arts Inc.

Church, A. 1941. *The Calculi of Lambda-Conversion*. Princeton, NJ:
Princeton University Press.

Cox, P. T., T. Pietrzykowski, and S. Matwin. 1984. “Concurrent Editing
and Executing in PROGRAPH on Graphical Microcomputers.” In *Proc. 24th
Intl. Symp. MIMI*, 140–46.

Crockford, D. 2006. “The Application/Json Media Type for JavaScript
Object Notation (JSON).” Request for Comments. RFC 4627.
<https://doi.org/10.17487/RFC4627>.

Ellis, T. O., J. F. Heafner, and W. L. Sibley. 1969. *The Grail Project:
An Experiment in Man-Machine Communications*. Santa Monica, CA: RAND
Corporation.

Ferraiolo, Jon. 2001. “Scalable Vector Graphics (SVG) 1.0
Specification.” World Wide Web Consortium.
<https://www.w3.org/TR/SVG10/>.

Fraser, N. 2015. “Ten Things We’ve Learned from Blockly.” In *IEEE
Blocks and Beyond*. <https://doi.org/10.1109/blocks.2015.7369000>.

Glinert, E. P., and S. Tanimoto. 1984. “Pict: An Interactive Graphical
Programming Language.” *Computer* 7 (25): 7--25.
<https://doi.org/10.1109/MC.1984.1658997>.

Griffiths, D. 2008. “Scheme Bricks.” Computer program.
<https://github.com/nebogeo/scheme-bricks/>.

Gruber, J. 2004. *Markdown*.
<https://daringfireball.net/projects/markdown/>.

Kimura, T., and P. McLain. 1986. “Show and Tell User’s Manual.”
Washington University. <https://doi.org/10.7936/K7QR4VF1>.

Laurson, M., and Jacques Duthen. 1989. “Patchwork: A Graphic Language in
preFORM.” In *Proc. ICMC*.
<https://hdl.handle.net/2027/spo.bbp2372.1989.042>.

Maloney, J. H., L. Burd, Y. Kafai, N. Rusk, B. Silverman, and M.
Resnick. 2004. “Scratch: A Sneak Preview.” In *Proc. International
Conference on Creating, Connecting and Collaborating Through Computing*,
104–9. <https://doi.org/10.1109/C5.2004.1314376>.

McCarthy, J. 1960. “Recursive Functions of Symbolic Expressions and
Their Computation by Machine.” *Communications of the ACM* 3 (4):
184–95. <https://doi.org/doi/10.1145/367177.367199>.

McCartney, J. 1996. “SuperCollider: A New Real-Time Synthesis Language.”
In *Proc. ICMC*. <http://hdl.handle.net/2027/spo.bbp2372.1996.078>.

Pausch, R., T. Burnette, A. C. Capeheart, M. Conway, D. Cosgrove, R.
DeLIne, J. Durbin, R. Gossweiler, S. Koga, and J. White. 1995. “Alice:
Rapid Prototyping System for Virtual Reality.” *IEEE Computer Graphics
and Applications* 15 (May): 8–11. <https://doi.org/10.1109/38.376600>.

Puckette, M. 1988. “The Patcher.” In *Proc. ICMC*, 420–29.
<http://hdl.handle.net/2027/spo.bbp2372.1988.046>.

———. 1991a. “Combining Event and Signal Processing in the Max Graphical
Programming Environment.” *Computer Music Journal* 15 (3): 68–77.
<https://doi.org/10.2307/3680767>.

———. 1991b. “FTS: A Real-Time Monitor for Muliprocessor Music
Synthesis.” *Computer Music Journal* 15 (3): 58–67.
<https://doi.org/10.2307/3680766>.

———. 1996. “Pure Data: Another Integrated Computer Music Environment.”
In *Proceedings of the Second Intercollege Computer Music Concerts*,
37–41.

Putnam, L. J. 2004. “SuperCollider One-Liners.” Computer program.
<https://w2.mat.ucsb.edu/l.putnam/sc3one/>.

Repenning, A. 1993. “Agentsheets: A Tool for Building Domain-Oriented
Dynamic, Visual Environments.” PhD thesis, USA: University of Colorado
at Boulder.

Rossberg, Andreas. 2019. “WebAssembly Core Specification.” World Wide
Web Consortium. <https://www.w3.org/TR/wasm-core-1/>.

Scaletti, C. 1987. “Kyma: An Object-Oriented Language for Music
Composition.” In *Proc. ICMC*, 49–56.
<http://hdl.handle.net/2027/spo.bbp2372.1987.007>.

Smith, D. 1975. “Pygmalion: A Creative Programming Environment.”
STAN-CS-75-499. Stanford University.

Smith, R. B. 1986. “The Alternate Reality Kit: An Animated Environment
for Creating Interactive Simulations.” In *Proc. IEEE Workshop on Visual
Languages*, 99–106.

Steinmetz, J. 2001. “Computers and Squeak as Environments for Learning.”
In *Squeak: Open Personal Computing for Multimedia*, edited by Mark
Guzdial and Kimberly Rose. Prentice Hall.

Sutherland, I. E. 1963. “Sketchpad: A Man-Machine Graphical
Communication System.” Technical Report 296. Cambridge, MA: MIT Lincoln
Laboratory.

Sutherland, W. 1966. “The on-Line Graphical Specification of Computer
Procedures.” PhD thesis, Massachusetts Institute of Technology.
<http://hdl.handle.net/1721.1/13474>.

Ungar, D. M., and R. B. Smith. 1991. “Self: The Power of Simplicity.”
*Lisp and Symbolic Computation* 4 (3): 187–205.
<https://doi.org/10.1007/bf01806105>.

Vose, G. M., and G. Williams. 1986. “LabVIEW: Laboratory Virtual
Instrument Engineering Workbench.” *Byte* 11: 84–92.

Warth, A., P. Dubroy, and T. Garnock-Jones. 2016. “Modular Semantic
Actions.” In *Proc. Symposium on Dynamic Languages*, 108–19.
<https://doi.org/10.1145/2989225.2989231>.

Wirfs-Brock, A., and B. Eich. 2020. “JavaScript: The First 20 Years.”
*Proc. ACM Program. Lang.*, HOPL, 4: 1–189.
<https://doi.org/10.1145/3386327>.

Yi, S., V. E. P. Lazzarini, and E. Costello. 2018. “WebAssembly
AudioWorklet Csound.” In *Proceedings of the International Web Audio
Conference*.
