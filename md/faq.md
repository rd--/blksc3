# FAQ

## What is Block SuperCollider?

Block SuperCollider is a visual programming language specialised for writing programs for the SuperCollider synthesiser.
There is a concise overview of Block SuperCollider in
[§1.01](https://blksc3.rohandrape.net/?e=help/Guide/1.01%20Block%20SuperCollider)
of the Guide.

## Where is JavaScript SuperCollider defined?

- <https://rohandrape.net/t/jssc3>

## Where is the Simple Programming Language defined?

- <https://rohandrape.net/t/spl>

## How are the notes texts defined?

For each `.json` help file there is a corresponding `.sl` file.
The notes are any text in the `.sl` following a line containing the string `---- notes.md ----`.
If there are no notes the `.sl` program is given.

## Where are the initials in the help files explained?

[§2.02](https://blksc3.rohandrape.net/?e=help/Guide/2.02%20Authors%2C%20Initials)
of the Guide lists the authors of the illustration programs.

## How are the toolboxes defined?

- <https://gitlab.com/rd--/blksc3/-/blob/main/json/CompleteToolbox.json>
- <https://gitlab.com/rd--/blksc3/-/blob/main/json/SmallToolbox.json>

## How is the colour scheme defined?

- <https://gitlab.com/rd--/blksc3/-/blob/main/json/ColourScheme.json>

## How are the "Programs", "Help", "Guide" and "Small Programs" menus defined?

- <https://gitlab.com/rd--/blksc3/-/blob/main/json/ProgramsMenu.json>
- <https://gitlab.com/rd--/blksc3/-/blob/main/json/HelpMenu.json>
- <https://gitlab.com/rd--/blksc3/-/blob/main/json/GuideMenu.json>
- <https://gitlab.com/rd--/blksc3/-/blob/main/json/SmallProgramsMenu.json>

## How is the layouts menu defined?

- <https://gitlab.com/rd--/blksc3/-/blob/main/json/Layouts.json>

## How is the program oracle defined?

- <https://gitlab.com/rd--/blksc3/-/blob/main/json/ProgramOracle.json>

## How are the block messages defined?

- <https://gitlab.com/rd--/blksc3/-/blob/main/json/SymbolicMessages.json>
- <https://gitlab.com/rd--/blksc3/-/blob/main/json/TextMessages.json>
