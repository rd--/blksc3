-- | Translator for simple .stc/.spl graphs to Blockly .xml notation.
--
-- In addition to the current .xml notation there is also a newer .json notation.
--

import Data.Char {- base -}
import Data.Either {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import Text.Printf {- base -}

import qualified Music.Theory.Json as Json {- hmt-base -}

import qualified Music.Theory.String as String {- hmt-base -}

import qualified Sound.Sc3.Ugen.Db as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Pseudo as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Record as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Rename as Db {- hsc3-db -}

import qualified Language.Smalltalk.Ansi as St {- stsc3 -}
import Language.Smalltalk.Ansi.Expr {- stsc3 -}
import qualified Language.Smalltalk.Stc.Translate as Stc {- stsc3 -}

-- * Util

-- | Is unary operator an event parameter?
is_event_param :: String -> Bool
is_event_param = flip elem (words "v w y x z o rx ry p px")

-- * Json

-- | Literal given type.  See lit_float_json, lit_int_json and lit_str_json for use.
lit_ty_json :: String -> String -> String -> (t -> Json.Value) -> t -> Json.Value
lit_ty_json blk_ty lit_ty field_ty enc n =
  Json.object
    [
      ( blk_ty
      , Json.object
          [ ("type", Json.string lit_ty)
          , ("fields", Json.object [(field_ty, enc n)])
          ]
      )
    ]

{- | Literal float.

>>> putStr $ Json.encode_value_str $ lit_float_json "shadow" 220
{"shadow":{"fields":{"NUM":220},"type":"math_number"}}
-}
lit_float_json :: String -> Double -> Json.Value
lit_float_json ty = lit_ty_json ty "math_number" "NUM" Json.double

{- | Literal integer.

>>> putStr $ Json.encode_value_str $ lit_int_json "shadow" 220
{"shadow":{"fields":{"NUM":220},"type":"math_number"}}
-}
lit_int_json :: String -> Integer -> Json.Value
lit_int_json ty = lit_ty_json ty "math_number" "NUM" Json.integer

{- | Literal string.

>>> putStr $ Json.encode_value_str $ lit_str_json "text" "string"
{"shadow":{"fields":{"TEXT":"text"},"type":"text"}}
-}
lit_str_json :: String -> String -> Json.Value
lit_str_json ty = lit_ty_json ty "text" "TEXT" Json.string

lit_json :: String -> St.Literal -> Json.Value
lit_json ty l =
  case l of
    St.NumberLiteral (St.Float n) -> lit_float_json ty n
    St.NumberLiteral (St.Int n) -> lit_int_json ty n
    St.StringLiteral s -> lit_str_json ty s
    St.SymbolLiteral s -> lit_str_json ty s
    St.ArrayLiteral a -> array_json (map (lit_json ty . fromLeft (error "non-literal in literal array")) a)
    _ -> error "lit_json"

{- | Ugen Json

>>> putStr $ Json.encode_value_str $ ugen_json "SinOsc" [lit_int_json "shadow" 110,lit_float_json "shadow" 0.5]
{"inline":true,"inputs":{"add":{"shadow":{"fields":{"NUM":0},"type":"math_number"}},"freq":{"shadow":{"fields":{"NUM":110},"type":"math_number"}},"mul":{"shadow":{"fields":{"NUM":1},"type":"math_number"}},"phase":{"shadow":{"fields":{"NUM":0.5},"type":"math_number"}}},"type":"Sc_SinOsc"}
-}
ugen_json :: String -> [Json.Value] -> Json.Value
ugen_json stcNm l =
  let nm = Db.sc3_ugen_initial_name stcNm
      (p, o) = ugen_param nm
      i = p ++ (if o then ["mul", "add"] else [])
      l' = l ++ (if o then [lit_int_json "shadow" 1, lit_int_json "shadow" 0] else [])
  in Json.object
      [ ("type", Json.string ("Sc_" ++ nm))
      , ("inline", Json.boolean True)
      , ("inputs", Json.object (zip i l'))
      ]

{-

>>> putStr $ Json.encode_value_str $ block_json_for "F" ["X", "Y"] [lit_int_json "block" 440, lit_int_json "block" 0]
{"inline":true,"inputs":{"X":{"block":{"fields":{"NUM":440},"type":"math_number"}},"Y":{"block":{"fields":{"NUM":0},"type":"math_number"}}},"type":"Sc_F"}
-}
block_json_for :: String -> [String] -> [Json.Value] -> Json.Value
block_json_for nm p d =
  Json.object
    [ ("type", Json.string ("Sc_" ++ nm))
    , ("inline", Json.boolean True)
    , ("inputs", Json.object (zip p d))
    ]

-- | Zero-indexed.
array_elem_json :: Int -> Json.Value -> Json.Association
array_elem_json k x = (printf "ADD%d" k, x)

{- | Array Json

>>> putStr $ Json.encode_value_str $ array_json [lit_int_json "shadow" 1,lit_float_json "shadow" 2.3,lit_int_json "shadow" 4]
{"block":{"extraState":{"itemCount":3},"inline":true,"inputs":{"ADD0":{"shadow":{"fields":{"NUM":1},"type":"math_number"}},"ADD1":{"shadow":{"fields":{"NUM":2.3},"type":"math_number"}},"ADD2":{"shadow":{"fields":{"NUM":4},"type":"math_number"}}},"type":"lists_create_with"}}
-}
array_json :: [Json.Value] -> Json.Value
array_json l =
  Json.object
    [
      ( "block"
      , Json.object
          [ ("type", Json.string "lists_create_with")
          , ("inline", Json.boolean True)
          , ("extraState", Json.object [("itemCount", Json.int (length l))])
          , ("inputs", Json.object (zipWith array_elem_json [0 ..] l))
          ]
      )
    ]

comment_json :: St.Comment -> Maybe Json.Value
comment_json c =
  if null c
    then Nothing
    else
      Just
        ( Json.object
            [ ("type", Json.string "Sc_Comment")
            ,
              ( "fields"
              , Json.object
                  [("COMMENT", Json.string (if last c == '\n' then init c else c))]
              )
            ]
        )

-- * Xml

lit_float_xml :: String -> Double -> String
lit_float_xml ty n = printf "<%s type='math_number'><field name='NUM'>%f</field></%s>" ty n ty

lit_int_xml :: String -> Integer -> String
lit_int_xml ty n = printf "<%s type='math_number'><field name='NUM'>%d</field></%s>" ty n ty

lit_str_xml :: String -> String -> String
lit_str_xml ty s = printf "<%s type='text'><field name='TEXT'>%s</field></%s>" ty s ty

lit_xml :: String -> St.Literal -> String
lit_xml ty l =
  case l of
    St.NumberLiteral (St.Float n) -> lit_float_xml ty n
    St.NumberLiteral (St.Int n) -> lit_int_xml ty n
    St.StringLiteral s -> lit_str_xml ty s
    St.SymbolLiteral s -> lit_str_xml ty s
    St.ArrayLiteral a -> array_xml (map (lit_xml ty . fromLeft (error "non-literal in literal array")) a)
    _ -> error "lit_xml"

{- | Ugen Param

>>> ugen_param "SinOsc"
(["freq","phase"],True)

>>> ugen_param "ControlIn"
-}
ugen_param :: String -> ([String], Bool)
ugen_param nm =
  case Db.u_lookup_cs nm of
    Just u -> (Db.u_input_names u, isJust (Db.ugen_outputs u))
    Nothing -> case Db.pseudo_ugen_db_lookup nm of
      Just (_, p, o, _, _) -> (p, o)
      Nothing -> error ("ugen_param: " ++ nm)

named_value_xml :: (String, String) -> String
named_value_xml (k, v) = printf "<value name='%s'>%s</value>" (map toUpper k) v

{- | Ugen Xml

>>> putStr $ ugen_xml "SinOsc" [lit_int_xml "shadow" 110,lit_float_xml "shadow" 0.5]
<block type='Sc_SinOsc' inline='true'>
<value name='FREQ'><shadow type='math_number'><field name='NUM'>110</field></shadow></value>
<value name='PHASE'><shadow type='math_number'><field name='NUM'>0.5</field></shadow></value>
<value name='MUL'><shadow type='math_number'><field name='NUM'>1</field></shadow></value>
<value name='ADD'><shadow type='math_number'><field name='NUM'>0</field></shadow></value>
</block>
-}
ugen_xml :: String -> [String] -> String
ugen_xml stcNm l =
  let nm = Db.sc3_ugen_initial_name stcNm
      (p, o) = ugen_param nm
      i = p ++ (if o then ["mul", "add"] else [])
      l' = l ++ (if o then [lit_int_xml "shadow" 1, lit_int_xml "shadow" 0] else [])
  in unlines
      [ printf "<block type='Sc_%s' inline='true'>" nm
      , String.unlinesNoTrailingNewline (map named_value_xml (zip i l'))
      , "</block>"
      ]

block_xml_for :: String -> [String] -> [String] -> String
block_xml_for nm p d =
  let l = concatMap named_value_xml (zip p d)
  in unlines
      [ printf "<block type='Sc_%s' inline='true'>" nm
      , l
      , "</block>"
      ]

{- | Some names are handled specially: {Overlap|XFade}Texture, SoundFileBuffer, Voicer, VoiceWriter

>>> putStr $ implicit_send_xml "SinOsc" ["440", "0"]
<block type='Sc_SinOsc' inline='true'>
<value name='FREQ'>440</value>
<value name='PHASE'>0</value>
<value name='MUL'><shadow type='math_number'><field name='NUM'>1</field></shadow></value>
<value name='ADD'><shadow type='math_number'><field name='NUM'>0</field></shadow></value>
</block>
-}
implicit_send_xml :: String -> [String] -> String
implicit_send_xml nm l =
  case (nm, l) of
    ("OverlapTexture", [_, _, _, _]) ->
      block_xml_for "OverlapTexture" ["PROC", "SUSTAINTIME", "TRANSITIONTIME", "OVERLAP"] l
    ("XFadeTexture", [_, _, _]) ->
      block_xml_for "XFadeTexture" ["PROC", "SUSTAINTIME", "TRANSITIONTIME"] l
    ("SoundFileBuffer", [_, _]) ->
      block_xml_for "SoundFileBuffer" ["SOUNDFILEID", "NUMBEROFCHANNELS"] l
    ("Voicer", [_, _]) ->
      block_xml_for "Voicer" ["COUNT", "PROC"] l
    ("VoiceWriter", [_, _]) ->
      block_xml_for "VoiceWriter" ["COUNT", "PROC"] l
    _ -> ugen_xml nm l

-- | Event parameters (v, w, x, y, z &etc)
event_param_xml :: String -> String -> String
event_param_xml o e =
  unlines
    [ "<block type='Sc_EventParam'>"
    , printf "<field name='PARAM'>%s</field>" o
    , printf "<value name='EVENT'>%s</value>" e
    , "</block>"
    ]

-- | Method names that belong to Array not UGen
array_proc_1 :: [String]
array_proc_1 = ["asLocalBuf", "choose", "catenate", "first", "mean", "reverse", "second", "size", "sum", "third", "transpose"]

{- | Some operators are handled specially.

1. dup -> Sc_ArrayFill
2. value -> Sc_Value0
3. splay2 -> Sc_Splay2
4. array operators -> Sc_ArrayProc1

>>> putStr $ uop_xml "MidiCps" (lit_int_xml "shadow" 60)
<block type='Sc_UnaryOp'>
<field name='OP'>MidiCps</field>
<value name='IN'><shadow type='math_number'><field name='NUM'>60</field></shadow></value>
</block>
-}
uop_xml :: String -> String -> String
uop_xml o e =
  case o of
    "dup" ->
      unlines
        [ "<block type='Sc_ArrayFill' inline='true'>"
        , printf "<value name='PROC'>%s</value>" e
        , printf "<value name='COUNT'>%s</value>" (lit_int_xml "block" 2)
        , "</block>"
        ]
    "value" ->
      unlines
        [ "<block type='Sc_Value0' inline='true'>"
        , printf "<value name='PROC'>%s</value>" e
        , "</block>"
        ]
    "splay2" ->
      unlines
        [ "<block type='Sc_Splay2' inline='true'>"
        , printf "<value name='INARRAY'>%s</value>" e
        , "</block>"
        ]
    _ ->
      let ty = if o `elem` array_proc_1 then "ArrayProc1" else "UnaryOp"
      in unlines
          [ printf "<block type='Sc_%s'>" ty
          , printf "<field name='OP'>%s</field>" o
          , printf "<value name='IN'>%s</value>" e
          , "</block>"
          ]

-- | Operator and method names that belong to Array not UGen
array_proc_2 :: [String]
array_proc_2 = ["++", "collect", "at"]

{- | Some operators are handled specially.

1. array operators -> ArrayProc2

>>> putStr $ binop_xml "+" (lit_int_xml "shadow" 60) (lit_float_xml "shadow" 0.75)
<block type='Sc_BinaryOp' inline='true'>
<field name='OP'>+</field>
<value name='LHS'><shadow type='math_number'><field name='NUM'>60</field></shadow></value>
<value name='RHS'><shadow type='math_number'><field name='NUM'>0.75</field></shadow></value>
</block>
-}
binop_xml :: String -> String -> String -> String
binop_xml o lhs rhs =
  let ty = if o `elem` array_proc_2 then "ArrayProc2" else "BinaryOp"
  in unlines
      [ printf "<block type='Sc_%s' inline='true'>" ty
      , printf "<field name='OP'>%s</field>" o
      , printf "<value name='LHS'>%s</value>" lhs
      , printf "<value name='RHS'>%s</value>" rhs
      , "</block>"
      ]

{- | Some operators are handled specially.

1. array operators -> ArrayProc2
2. dup -> ArrayFill
3. to -> ArrayFromTo
4. timesRepeat -> TimesRepeat
5. value -> Value1
-}
keybinop_xml :: String -> String -> String -> String
keybinop_xml msg lhs rhs =
  case msg of
    "dup:" ->
      printf
        "<block type='Sc_ArrayFill' inline='true'>\n<value name='PROC'>%s</value>\n<value name='COUNT'>%s</value>\n</block>"
        lhs
        rhs
    "timesRepeat:" ->
      printf
        "<block type='Sc_TimesRepeat'>\n<value name='COUNT'>%s</value>\n<value name='PROC'>%s</value>\n</block>"
        lhs
        rhs
    "to:" ->
      printf
        "<block type='Sc_ArrayFromTo' inline='true'>\n<value name='FROM'>%s</value>\n<value name='TO'>%s</value>\n</block>"
        lhs
        rhs
    "value:" ->
      printf
        "<block type='Sc_Value1' inline='true'>\n<value name='PROC'>%s</value>\n<value name='VALUE'>%s</value>\n</block>"
        lhs
        rhs
    _ ->
      let ty = if (init msg) `elem` array_proc_2 then "ArrayProc2" else "BinaryOp"
      in printf
          "<block type='Sc_%s' inline='true'>\n<field name='OP'>%s</field>\n<value name='LHS'>%s</value>\n<value name='RHS'>%s</value>\n</block>"
          ty
          (init msg)
          lhs
          rhs

keyternaryop_xml :: String -> String -> String -> String -> String
keyternaryop_xml msg p1 p2 p3 =
  case msg of
    "value:value:" ->
      printf
        "<block type='Sc_Value2' inline='true'>\n<value name='PROC'>%s</value>\n<value name='VALUE1'>%s</value>\n<value name='VALUE2'>%s</value>\n</block>"
        p1
        p2
        p3
    _ -> error ("keyternaryop_xml: " ++ show [msg, p1, p2, p3])

{- | Variable declaration

>>> var_decl ["x","o"]
"<variables><variable>x</variable><variable>o</variable></variables>"
-}
var_decl :: [String] -> String
var_decl v =
  if null v
    then ""
    else printf "<variables>%s</variables>" (concatMap (\x -> concat ["<variable>", x, "</variable>"]) v)

{- | Variable set

>>> putStr $ var_set "z" (lit_float_xml "shadow" 0.75)
<block type='variables_set'>
<field name='VAR'>z</field>
<value name='VALUE'><shadow type='math_number'><field name='NUM'>0.75</field></shadow></value>
</block>
-}
var_set :: String -> String -> String
var_set =
  printf
    "<block type='variables_set'>\n<field name='VAR'>%s</field>\n<value name='VALUE'>%s</value>\n</block>"

var_set_then :: String -> String -> String -> String
var_set_then =
  printf
    "<block type='variables_set'>\n<field name='VAR'>%s</field>\n<value name='VALUE'>%s</value>\n<next>%s</next>\n</block>"

-- | Booleans are special cases
var_get :: String -> String
var_get x =
  case x of
    "true" -> "<block type='logic_boolean'>\n<field name='BOOL'>TRUE</field>\n</block>"
    "false" -> "<block type='logic_boolean'>\n<field name='BOOL'>FALSE</field>\n</block>"
    _ -> printf "<block type='variables_get'>\n<field name='VAR'>%s</field>\n</block>" x

array_elem_xml :: Int -> String -> String
array_elem_xml k x = printf "<value name='ADD%d'>%s</value>" k x

{- | Array Xml

>>> putStr $ array_xml [lit_int_xml "shadow" 1,lit_float_xml "shadow" 2.3,lit_int_xml "shadow" 4]
<block type='lists_create_with' inline='true'>
<mutation items='3'></mutation>
<value name='ADD0'>{"shadow": {"type": "math_number", "fields": {"NUM": 1}}}</value>
<value name='ADD1'>{"shadow": {"type": "math_number", "fields": {"NUM": 2.3}}}</value>
<value name='ADD2'>{"shadow": {"type": "math_number", "fields": {"NUM": 4}}}</value>
</block>
-}
array_xml :: [String] -> String
array_xml l =
  printf
    "<block type='lists_create_with' inline='true'>\n<mutation items='%d'></mutation>\n%s</block>"
    (length l)
    (unlines (zipWith array_elem_xml [0 ..] l))

-- | If the last expression is an assignment (ie. if the rhs is null), return the variable getter.
expr_group_assignments :: [Expr] -> ([Expr], Expr)
expr_group_assignments e =
  case span exprIsAssignment e of
    (a, []) -> (a, Identifier (fromMaybe (error "expr_group_assignments: id?") (assignmentIdentifier (last a))))
    (a, [v]) -> (a, v)
    _ -> error "expr_group_assignments?"

{- | Xml for Proc (Lambda).
     If the last expression is an assignment return the variable assigned to.
-}
proc_xml :: [String] -> [String] -> [Expr] -> String
proc_xml a _v e =
  case (a, expr_group_assignments e) of
    ([], ([], r)) ->
      printf
        "<block type='Sc_Proc0' inline='true'>\n<value name='RETURN'>%s</value>\n</block>"
        (expr_xml r)
    ([], (s, r)) ->
      printf
        "<block type='Sc_Proc0Stm'>\n<value name='STATEMENTS'>%s</value>\n<value name='RETURN'>%s</value>\n</block>"
        (assign_seq_xml s)
        (expr_xml r)
    ([a1], ([], r)) ->
      printf
        "<block type='Sc_Proc1' inline='true'>\n<value name='VAR'>%s</value>\n<value name='RETURN'>%s</value>\n</block>"
        (var_get a1)
        (expr_xml r)
    ([a1], (s, r)) ->
      printf
        "<block type='Sc_Proc1Stm'>\n<value name='VAR'>%s</value>\n<value name='STATEMENTS'>%s</value>\n<value name='RETURN'>%s</value>\n</block>"
        (var_get a1)
        (assign_seq_xml s)
        (expr_xml r)
    ([a1, a2], ([], r)) ->
      printf
        "<block type='Sc_Proc2' inline='true'>\n<value name='VAR1'>%s</value>\n<value name='VAR2'>%s</value>\n<value name='RETURN'>%s</value>\n</block>"
        (var_get a1)
        (var_get a2)
        (expr_xml r)
    ([a1, a2], (s, r)) ->
      printf
        "<block type='Sc_Proc2Stm'>\n<value name='VAR1'>%s</value>\n<value name='VAR2'>%s</value>\n<value name='STATEMENTS'>%s</value>\n<value name='RETURN'>%s</value>\n</block>"
        (var_get a1)
        (var_get a2)
        (assign_seq_xml s)
        (expr_xml r)
    _ -> error (show ("proc_xml: not 0, 1 or 2 argument", a, e))

comment_xml :: St.Comment -> String
comment_xml c =
  if null c
    then ""
    else printf "<block type='Sc_Comment'>\n<field name='COMMENT'>%s</field>\n</block>" (if last c == '\n' then init c else c)

expr_xml :: Expr -> String
expr_xml e =
  case e of
    Array l -> array_xml (map expr_xml l)
    Assignment p q -> var_set p (expr_xml q)
    Identifier x -> var_get x
    Literal l -> lit_xml "block" l
    Send (Identifier u) (Message (St.KeywordSelector "apply:" 1) [Array l]) -> implicit_send_xml u (map expr_xml l) -- Saw(440)
    Send r (Message (St.UnarySelector m) []) -> (if is_event_param m then event_param_xml else uop_xml) m (expr_xml r) -- 60.MidiCps
    Send lhs (Message (St.BinarySelector m) [rhs]) -> binop_xml m (expr_xml lhs) (expr_xml rhs) -- 1 + 2
    Send lhs (Message (St.KeywordSelector m 1) [rhs]) -> keybinop_xml m (expr_xml lhs) (expr_xml rhs) -- 1.Max(2)
    Send p1 (Message (St.KeywordSelector m 2) [p2, p3]) -> keyternaryop_xml m (expr_xml p1) (expr_xml p2) (expr_xml p3) -- 1.Max(2)
    Lambda _ a v (e_seq, Nothing) -> proc_xml a v e_seq
    Init c _ s -> maybe "" comment_xml c ++ expr_seq_xml s
    _ -> error ("expr_xml: " ++ show e)

expr_seq_xml :: [Expr] -> String
expr_seq_xml = concatMap expr_xml

assign_seq_xml :: [Expr] -> String
assign_seq_xml e_seq =
  case e_seq of
    [Assignment p q] -> var_set p (expr_xml q)
    (Assignment p q : e_rem) -> var_set_then p (expr_xml q) (assign_seq_xml e_rem)
    _ -> error ("assign_seq_xml: " ++ show e_seq)

in_xml :: String -> String
in_xml x = unlines ["<xml>", x ++ "</xml>"]

-- | .stc files may have .md notes sections, discard these.
extract_stc_graph :: String -> String
extract_stc_graph = unlines . takeWhile (not . isPrefixOf "{- ----") . lines

{- | Stc to Xml

>>> rw = putStr . stc_to_xml
>>> rw "5.Abs"
<xml>
<block type='Sc_UnaryOp'>
<field name='OP'>Abs</field>
<value name='IN'><block type='math_number'><field name='NUM'>5</field></block></value>
</block>
</xml>

>>> rw "SinOsc(440, 0)"
<xml>
<block type='Sc_SinOsc' inline='true'>
<value name='FREQ'><block type='math_number'><field name='NUM'>440</field></block></value>
<value name='PHASE'><block type='math_number'><field name='NUM'>0</field></block></value>
<value name='MUL'><shadow type='math_number'><field name='NUM'>1</field></shadow></value>
<value name='ADD'><shadow type='math_number'><field name='NUM'>0</field></shadow></value>
</block>
</xml>

>>> rw "SinOsc(440, 0).Abs"
<xml>
<block type='Sc_UnaryOp'>
<field name='OP'>Abs</field>
<value name='IN'><block type='Sc_SinOsc' inline='true'>
<value name='FREQ'><block type='math_number'><field name='NUM'>440</field></block></value>
<value name='PHASE'><block type='math_number'><field name='NUM'>0</field></block></value>
<value name='MUL'><shadow type='math_number'><field name='NUM'>1</field></shadow></value>
<value name='ADD'><shadow type='math_number'><field name='NUM'>0</field></shadow></value>
</block>
</value>
</block>
</xml>

>>> rw "{ Rand(0, 1) }"
<xml>
<block type='Sc_Proc0' inline='true'>
<value name='RETURN'><block type='Sc_Rand' inline='true'>
<value name='LO'><block type='math_number'><field name='NUM'>0</field></block></value>
<value name='HI'><block type='math_number'><field name='NUM'>1</field></block></value>
<value name='MUL'><shadow type='math_number'><field name='NUM'>1</field></shadow></value>
<value name='ADD'><shadow type='math_number'><field name='NUM'>0</field></shadow></value>
</block>
</value>
</block></xml>

>> rw "{ Rand(0, 1) }.dup"
<xml>
<block type='Sc_ArrayFill' inline='true'>
<value name='PROC'><block type='Sc_Proc0' inline='true'>
<value name='RETURN'><block type='Sc_Rand' inline='true'>
<value name='LO'><block type='math_number'><field name='NUM'>0</field></block></value>
<value name='HI'><block type='math_number'><field name='NUM'>1</field></block></value>
<value name='MUL'><shadow type='math_number'><field name='NUM'>1</field></shadow></value>
<value name='ADD'><shadow type='math_number'><field name='NUM'>0</field></shadow></value>
</block>
</value>
</block></value>
<value name='COUNT'><block type='math_number'><field name='NUM'>2</field></block></value>
</block>
</xml>

>>> rw "{ Rand(0, 1) }.dup(5)"
<xml>
<block type='Sc_ArrayFill' inline='true'>
<value name='PROC'><block type='Sc_Proc0' inline='true'>
<value name='RETURN'><block type='Sc_Rand' inline='true'>
<value name='LO'><block type='math_number'><field name='NUM'>0</field></block></value>
<value name='HI'><block type='math_number'><field name='NUM'>1</field></block></value>
<value name='MUL'><shadow type='math_number'><field name='NUM'>1</field></shadow></value>
<value name='ADD'><shadow type='math_number'><field name='NUM'>0</field></shadow></value>
</block>
</value>
</block></value>
<value name='COUNT'><block type='math_number'><field name='NUM'>5</field></block></value>
</block></xml>

>>> rw "1.to(9)"
<xml>
<block type='Sc_ArrayFromTo' inline='true'>
<value name='FROM'><block type='math_number'><field name='NUM'>1</field></block></value>
<value name='TO'><block type='math_number'><field name='NUM'>9</field></block></value>
</block></xml>
-}
stc_to_xml :: String -> String
stc_to_xml = in_xml . expr_xml . Stc.stcToExpr

{- | Spl to Xml

>>> rw = putStr . spl_to_xml
>>> rw "{ :tr | TRand(0, 1, tr) }"
<xml>
<block type='Sc_Proc1' inline='true'>
<value name='VAR'><block type='variables_get'>
<field name='VAR'>tr</field>
</block></value>
<value name='RETURN'><block type='Sc_TRand' inline='true'>
<value name='LO'><block type='math_number'><field name='NUM'>0</field></block></value>
<value name='HI'><block type='math_number'><field name='NUM'>1</field></block></value>
<value name='TRIG'><block type='variables_get'>
<field name='VAR'>tr</field>
</block></value>
<value name='MUL'><shadow type='math_number'><field name='NUM'>1</field></shadow></value>
<value name='ADD'><shadow type='math_number'><field name='NUM'>0</field></shadow></value>
</block>
</value>
</block></xml>

>>> rw "[1, 2, 3].collect { :i | i * i }"
<xml>
<block type='Sc_ArrayProc2' inline='true'>
<field name='OP'>collect</field>
<value name='LHS'><block type='lists_create_with' inline='true'>
<mutation items='3'></mutation>
<value name='ADD0'><block type='math_number'><field name='NUM'>1</field></block></value>
<value name='ADD1'><block type='math_number'><field name='NUM'>2</field></block></value>
<value name='ADD2'><block type='math_number'><field name='NUM'>3</field></block></value>
</block></value>
<value name='RHS'><block type='Sc_Proc1' inline='true'>
<value name='VAR'><block type='variables_get'>
<field name='VAR'>i</field>
</block></value>
<value name='RETURN'><block type='Sc_BinaryOp' inline='true'>
<field name='OP'>*</field>
<value name='LHS'><block type='variables_get'>
<field name='VAR'>i</field>
</block></value>
<value name='RHS'><block type='variables_get'>
<field name='VAR'>i</field>
</block></value>
</block>
</value>
</block></value>
</block></xml>
-}
spl_to_xml :: String -> String
spl_to_xml = in_xml . expr_xml . Stc.splToExpr

graph_file_to_xml_file :: (String -> Expr) -> FilePath -> IO ()
graph_file_to_xml_file trs fn = do
  let dir = "/home/rohan/sw/blksc3/help/"
  txt <- readFile (dir ++ fn)
  writeFile (dir ++ fn ++ ".xml") (in_xml (expr_xml (trs (extract_stc_graph txt))))

-- | Stc file to Xml file
stc_file_to_xml_file :: FilePath -> IO ()
stc_file_to_xml_file = graph_file_to_xml_file Stc.stcToExpr

{- | Stc file to Xml file

> spl_file_to_xml_file "tmp/Hy - Tw 1601331785409134592.sl"
> spl_file_to_xml_file "tmp/Io - 6353.sl"
> spl_file_to_xml_file "tmp/F0 - Tw 0301.sl"
> spl_file_to_xml_file "tmp/Jrhb - Gcd.sl"
-}
spl_file_to_xml_file :: FilePath -> IO ()
spl_file_to_xml_file = graph_file_to_xml_file Stc.splToExpr
