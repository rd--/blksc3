/* Babbling Brook (Jmcc) #Sc3 */
let p = {
	Rhpf(
		OnePole(BrownNoise(), 0.99),
		Lpf(BrownNoise(), 14) * 400 + 500,
		0.03
	) * 0.06
} ! 2;
let q = {
	Rhpf(
		OnePole(BrownNoise(), 0.99),
		Lpf(BrownNoise(), 20) * 800 + 1000,
		0.01
	) * 0.10
} ! 2;
p + q
