/* Sprinkler mouse (Jmcc) #1 */
Bpz2(
	WhiteNoise()
	*
	LfPulse(
		MouseX(0.2, 50, 0, 0.2),
		0,
		0.25
	) * 0.1
)

# Annotation

Mouse control.
