/* LfSaw.2 */
var lfo = LfSaw(2, 0) * -100 + 600;
SinOsc(lfo, 0) * 0.1

# LfSaw

As an lfo, courtesy [Coc](https://github.com/cianoc/supercollider_fragments)
