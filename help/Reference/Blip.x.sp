/* Blip ; Voicer */
Voicer(1, 16) { :e |
    Blip(e.p.UnitCps, e.y * 9) * e.w * e.z
}.Splay2
