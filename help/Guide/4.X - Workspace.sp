/* 4.X - Workspace */
Resonz(
	PinkNoise(),
	LfNoise2([0.11 0.77]) * 333 + 555,
	[0.11 0.33]
) * 0.1

# Annotation

Blocks can be moved about the workspace and connected and disconnected from each other using the mouse.
The _Fit_ button resizes the block program so that it fits inside the workspace.
