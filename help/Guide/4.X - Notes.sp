/* 4.X - Notes */
SinOsc(
	LfCub(
		[1.3 5.7],
		0
	) * 135 + 357 % [123 345],
	0
) * 0.1

# Annotation

Programs may include notes written in a simple markup language.
This text is such a note.
Notes are displayed to the right of the workspace.
Layouts allocate more or less space to the notes,
and set the font size.
