/* Tutorial */
PinkNoise() * 0.1

# Annotation

The _Capsule Tutorial_,
§4 of the _Guide_,
consists of a sequence of very short texts,
each annotating a brief program.

In particular,
capsule tutorial texts are written so they can be displayed using the _%×%|50_ screen layouts,
where half the space is devoted to the notes,
which are shown in a large font.
