/* Guide Files */
PinkNoise() * 0.1

# Guide Files

This menu has an online guide to the system.

The Guide consists of a sequence of small example programs each with extensive notes.

This drawing plays quiet pink noise in the left channel.
