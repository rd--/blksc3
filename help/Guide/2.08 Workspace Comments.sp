/* Workspace Comments */
Resonz(
	Dust([0.3, 3]),
	[333, 999],
	[0.03, 0.003]
) * 9

# Workspace Comments

Workspace comments can be added to a program using the _Add Comment_ item in the workspace context menu.

However unlike the attached notes, which are written in a simple markup language, workspace comments are plain text only.

This program plays two channels of randomly occuring resonated impulses, ocurring quicker and resonated at a higher pitch in the second channel.
