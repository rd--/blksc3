/* 4.X - Functions */
{
	Blip(
		ExpRand(99, 222),
		4
	) * 0.1
} !^ 23

# Annotation

The λ family of blocks represent procedures.
! constructs a list by evaluating a λ block the specified number of times.
◠ arranges the list of sounds equidistantly across the stereo field.
_Replace_ crossfades between sounds.
