/* 4.X - Black and White */
SinOsc(
	Latch(
		SinOsc(12.3, 0),
		Impulse([9.9 11.1], 0)
	) * 234 + 345,
	0
) * 0.1

# Annotation

There is a black and white, or grey, or outline, drawing mode.
These drawings may be more suitable for printing or for non-colour displays,
and are selected using the ⬜ button.
