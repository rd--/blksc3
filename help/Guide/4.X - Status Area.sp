/* 4.X - Status Area */
let x = { WhiteNoise() * 0.05 } ! 2;
{
	Resonz(
		x,
		LfNoise2(0.33) * 333 + 777,
		LfNoise2(0.77) * 0.07 + 0.09
	)
} !+ 10

# Annotation

The _Status Area_ is located to the right of the _Begin_ button.
It shows the number of sounding unit generators.
This program describes a graph with eighty-three vertices.
