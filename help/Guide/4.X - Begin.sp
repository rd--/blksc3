/* 4.X - Begin */
{
	Hpf(
		GrayNoise() * 0.1,
		LfNoise2(3) * 6666 + 9999
	)
} ! 2

# Annotation

_Block SuperCollider_
is a visual editor for
_SuperCollider_ synthesiser programs.

Ordinarily,
programs are sent to an in-process copy of the SuperCollider synthesiser,
which can be started by pressing the _Begin_ button.
