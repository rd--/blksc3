/* 4.X - Block Colours */
...

# Annotation

The blocks in each category are all drawn in the same colour,
shown to the left of the category name.
Categories are grouped into families,
also indicated by colouring.
