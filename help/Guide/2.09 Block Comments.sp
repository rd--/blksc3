# Block Comments

Block comments can be added to a program using the _Add Comment_ item in the block context menu.
If a block has a comment a ? tool is added to the block that can be used to open the comment text.
In this program the ⋏ (_Resonz_) block has a comment attached.

However unlike the attached notes, which are written in a simple markup language, workspace comments are plain text only.

Block comments retain their dimensions when restored,
however unlike workspace comments they not retain the location they have been positioned at.

This program plays two channels of randomly occurring resonated impulses,
occurring quicker and resonated at a higher pitch in the second channel.
