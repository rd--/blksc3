/* 4.X - Shadow Block */
Resonz(
	PinkNoise(),
	999,
	0.01
)

# Annotation

A shadow block is an editable but non-movable block connected to another block.
Toolbox entries ordinarily have shadow blocks at each input specifying a _default value_.
Blocks can be placed on top of shadow blocks to overwrite them,
the ν and ¹⁄𝑄 inputs have shadow blocks below the number blocks.



