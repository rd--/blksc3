/* 4.X - Block Editor */
Rlpf(
	WhiteNoise() * 0.1,
	LfGauss(
		3.3,
		0.33,
		0, 1, 0
	) * 333 + 666,
	0.22
)

# Annotation

The block editor has two parts,
a _Workspace_ where the drawing is made,
and a _Toolbox_ from which the blocks are taken.
