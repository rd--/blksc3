/* 4.X - Binary Operator */
...

# Annotation

A binary operator is an operator with two operands, or a procedure of two arguments.
In some contexts it particularly refers to a procedure that is written using infix notation.
The figure below is the block diagram of 3 + 4.
