/* 4.X - Assignment */
let p = { WhiteNoise() * 0.02 } ! 2;
p + CombC(
	p,
	0.1,
	LfNoise1(10) * 0.08 + 0.08,
	-10000
)

# Annotation

The assignment block ≔ sets the value of a variable.
Because block drawings are of trees and not graphs,
if the answer of a block is to be used in more than one place it must be named.
[Lpj - No. 12(d)](https://w2.mat.ucsb.edu/l.putnam/sc3one/index.html)
