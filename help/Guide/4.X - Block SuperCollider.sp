/* 4.X - Block SuperCollider */
SinOsc(440, 0) * 0.1

# Annotation

Programs are written as sets of interlocking graphical blocks.
Each block represents one element of the program.
To listen to a program press the _Play_ button,
to stop listening press _Reset_.

The ∿ block represents a sine tone oscillator,
and this program generates a quiet sine tone at 440 _hz_.
