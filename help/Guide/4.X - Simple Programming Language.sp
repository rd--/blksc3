/* 4.X - Simple Programming Language */
Resonz(
	WhiteNoise(),
	333,
	0.33
) * 0.1

# Annotation

_Simple Programming Language_ (Sᴘʟ),
or _Simple Language_ (Sʟ),
is the language that _Block SuperCollider_ programs are drawings of,
and that the drawings are translated into when evaluated.
The text below is the Sᴘʟ translation of the drawing at the left.
