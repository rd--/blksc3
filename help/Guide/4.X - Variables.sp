/* 4.X - Variables */
...

# Annotation

Variables are created using the _Create Variable_ entry in the _Variable_ category of the toolbox,
which also contains the variable reference and assignment blocks.

The figure below indicates the two variable related blocks, variable assignment and variable reference.
