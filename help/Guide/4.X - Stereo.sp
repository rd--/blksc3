/* 4.X - Stereo */
[
	PinkNoise() * 0.1,
	BrownNoise() * 0.05
]

# Annotation

A stereo sound can be made by writing a list of two mono sounds.

The ⟦⟧ block constructs lists,
the ⚙ button allows slots to be added and removed.

The ⍰𝒑 block generates pink noise,
the ⍰𝒃 block generates brown noise.


