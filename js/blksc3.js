import * as sc from '../lib/scsynth-wasm-builds/lib/jssc3/dist/jssc3.js';
import * as sl from '../lib/scsynth-wasm-builds/lib/spl/dist/sl.js';

import { initCodeGen } from './CodeGenerator.js';
import { initCodeGenUgen } from './UgenCodeGenerator.js';
import { loadNotes } from './notes.js';

function addWorkspaceEnvelope(input) {
	const gate = sc.NamedControl('workspaceGate', 1);
	const attackTime = sc.NamedControl('workspaceAttackTime', 0.01);
	const releaseTime = sc.NamedControl('workspaceReleaseTime', 0.1);
	const env = sc.EnvAsr(attackTime, 1, releaseTime, 'sin');
	const envelope = sc.EnvGen(gate, 1, 0, 1, 2, sc.envCoord(env));
	return sc.Mul(input, envelope);
}

function setCssProperties(settings) {
	const r = document.querySelector(':root');
	for (const key in settings) {
		const value = settings[key];
		r.style.setProperty(key, value);
	}
}

const blksc3Theme = Blockly.Theme.defineTheme(
	'blksc3',
	{
		'base': Blockly.Themes.Classic,
		'fontStyle': {
			'family': 'CM059, Georgia, serif',
			'weight': 'normal',
			'size': 14
		}
	}
);

export class Blk {
	constructor(Blockly, withUiCtl, trackHistory) {
		this.Blockly = Blockly;
		this.trackHistory = trackHistory; // boolean
		this.config = null;
		this.workspace = null;
		this.layouts = null;
		this.naming = 'Symbolic';
		this.whichToolbox = 'Complete';
		this.whichColourScheme = 'Pretty';
		this.colourSaturation = 0.25;
		this.init(withUiCtl);
		this.programOracle = [];
	}

	init(withUiCtl) {
		initCodeGen(Blockly);
		initCodeGenUgen(Blockly);
		this.Blockly.ContextMenuItems.registerCommentOptions();
		this.initWorkspaceContextMenu();
		// https://developers.google.com/blockly/guides/create-custom-blocks/block-colour#colour_references
		this.loadBlockMessages('json/ColourScheme.json');
		this.setPrettyColours();
		this.loadBlockMessages(`json/${this.naming}Messages.json`);
		this.loadBlockDefinitions('json/BlockDefinitions.json');
		this.loadBlockDefinitions('json/UgenBlockDefinitions.json');
		this.loadProgramOracle('json/ProgramOracle.json');
		this.loadToolbox('json/CompleteToolbox.json', () => {
			this.workspace.addChangeListener(this.onWorkspaceChange());
			this.maybeLoadHelpFileFromUrlParam('e');
		});
		sc.connectButtonToInput('jsonInputFileSelect', 'jsonInputFile'); // Initialise .json file selector
		this.initMenus();
		this.loadMenus();
		sc.userPrograms.storageKey = 'blksc3UserPrograms/json';
		sc.userProgramsMenuInit(
			'userMenu',
			(jsonText) => this.loadProgramText(jsonText),
		);
		sc.selectOnChange('actionsMenu', function (menuElement, entryName) {
			sc.userActionDo(entryName, 'userMenu', 'userProgramArchiveFile');
			menuElement.selectedIndex = 0;
		});
		this.layoutMenuInit();
		if (withUiCtl) {
			sc.fetchUtf8('html/ui-ctl.html', { cache: 'no-cache' })
				.then(sc.setterForInnerHtmlOf('uiCtlContainer'));
		};
	}

	initMenus() {
		function graphMenuInit(menuId, graphDir, loadProc) {
			sc.menuOnChangeWithOptionValue(menuId, (optionValue) => {
				const graphPath = `help/${graphDir}/${optionValue}`;
				// console.debug(graphPath);
				loadProc(graphPath);
			});
		}
		graphMenuInit(
			'programsMenu',
			'Program',
			(path) => this.loadHelpGraph(path),
		);
		graphMenuInit('helpMenu', 'Reference', (path) => this.loadHelpGraph(path));
		graphMenuInit('guideMenu', 'Guide', (path) => this.loadHelpGraph(path));
		graphMenuInit(
			'smallProgramsMenu',
			'Program',
			(path) => this.loadHelpGraph(path),
		);
	}

	loadMenus() {
		sc.selectClearFrom('programsMenu', 0);
		sc.fetchJson('json/ProgramsMenu.json', { cache: 'no-cache' })
			.then((json) =>
				sc.selectAddKeysAsOptions('programsMenu', json.programsMenu)
			);
		sc.selectClearFrom('helpMenu', 0);
		sc.fetchJson('json/HelpMenu.json', { cache: 'no-cache' })
			.then((json) => sc.selectAddKeysAsOptions('helpMenu', json.helpMenu));
		sc.selectClearFrom('guideMenu', 0);
		sc.fetchJson('json/GuideMenu.json', { cache: 'no-cache' })
			.then((json) => sc.selectAddKeysAsOptions('guideMenu', json.guideMenu));
		sc.selectClearFrom('smallProgramsMenu', 0);
		sc.fetchJson('json/SmallProgramsMenu.json', { cache: 'no-cache' })
			.then((json) =>
				sc.selectAddKeysAsOptions('smallProgramsMenu', json.smallProgramsMenu)
			);
	}

	initConfig(toolbox) {
		this.config = {
			media: 'lib/scsynth-wasm-builds/lib/ext/blockly-11.2.1/media/',
			renderer: 'thrasos', // geras thrasos zelos
			theme: blksc3Theme,
			sounds: false,
			toolbox: toolbox,
			rtl: false,
			move: {
				scrollbars: {
					horizontal: true,
					vertical: true,
				},
				drag: true,
				wheel: false,
			},
			zoom: {
				controls: true, // missing sprites?
				wheel: true,
				startScale: 1.0,
				maxScale: 3,
				minScale: 0.3,
				scaleSpeed: 1.2,
				pinch: true,
			},
			trashcan: false,
		};
	}

	// Setup workspace on loading a new program.
	// Resets to unit scale and centers blocks.
	onProgramLoad() {
		this.workspace.setScale(1);
		this.workspace.scrollCenter();
	}

	// Load program from .json definition.
	// Clears any existing blocks.
	loadProgramText(jsonText) {
		this.workspace.clear();
		this.Blockly.serialization.workspaces.load(
			JSON.parse(jsonText),
			this.workspace,
		);
		this.onProgramLoad();
	}

	// Read selected .json file from file input.
	readFileInputJson(inputId) {
		const inputFile = sc.getFileInputFile(inputId, 0);
		inputFile.text().then((jsonText) => this.loadProgramText(jsonText));
	}

	/*
	  c.f. https://github.com/google/blockly/issues/3921
	  c.f. https://groups.google.com/g/blockly/c/GC5TsBUVVbE/
	*/
	displayScrollbars(showScrollbars) {
		const displayValue = showScrollbars ? '' : 'none';
		this.workspace.scrollbar.vScroll.svgHandle.style.display = displayValue;
		this.workspace.scrollbar.hScroll.svgHandle.style.display = displayValue;
	}

	// Set properties given layout configuration name.
	setLayout(configName) {
		// console.debug(`setLayout: ${configName}`);
		if (configName) {
			const w = document.getElementById('blocklyContainer');
			const n = document.getElementById('blkNotes');
			const e = document.getElementById('blkEdit');
			const o = this.layouts[configName];
			w.style.height = o.workspaceHeight;
			w.style.width = o.workspaceWidth;
			n.style.left = o.notesLeft;
			n.style.width = o.notesWidth;
			n.style.display = (o.notesWidth === "0") ? "none" : "block";
			n.style['font-size'] = o.notesFontSize;
			n.style.height = o.workspaceHeight;
			e.style.top = o.editTop;
			e.style.height = o.editHeight;
			this.Blockly.svgResize(this.workspace);
		}
	}

	/* Set event listener for layout menu.

	   %×% = proportional
	   1366×768 (x270) =16/9
	   1440×900 (macbook/3) = 8/5
	   1680×1050 (macbook/4) = 8/5
	   1920×1080 (x1) = 16/9
	*/
	layoutMenuInit() {
		const select = document.getElementById('blkLayoutMenu');
		select.addEventListener('change', (e) => this.setLayout(e.target.value));
		sc.fetchJson('json/Layouts.json', { cache: 'no-cache' })
			.then((obj) => {
				this.layouts = obj;
				console.log('layoutMenuInit', obj);
				Object.keys(obj).forEach((k) => {
					const e = new Option(k, k);
					select.add(e);
				});
			});
	}

	// Configure and inject Blockly given toolbox definition.
	injectWithToolbox(toolbox, onCompletion) {
		this.initConfig(toolbox);
		this.workspace = this.Blockly.inject('blocklyContainer', this.config);
		this.displayScrollbars(false);
		// console.debug('injectWithToolbox: injection completed');
		onCompletion(this);
	}

	initWorkspaceContextMenu() {
		const makeEntry = (name, handler) => {
			const item = {
				displayText: name,
				id: 'sc_' + name,
				weight: 100,
				preconditionFn: (_scope) => {
					return 'enabled';
				},
				callback: (_scope) => {
					handler();
				},
				scopeType: this.Blockly.ContextMenuRegistry.ScopeType.WORKSPACE,
			};
			this.Blockly.ContextMenuRegistry.registry.register(item);
		};
		makeEntry('Play', () => {
			this.playCode();
		});
		makeEntry('Replace', () => {
			this.replaceCode();
		});
		makeEntry('Reset', () => {
			globalScSynth.reset();
		});
	}

	getCodeSl() {
		const slText = this.Blockly.JavaScript.workspaceToCode(this.workspace);
		// console.debug('getCodeSl', slText);
		return slText;
	}

	getCodeJs() {
		const slText = this.getCodeSl();
		console.debug(`getCodeJs: slText: ${slText}`);
		const jsText = sl.rewriteSlToJs(slText);
		// console.debug(`getCodeJs: jsText: ${jsText}`);
		return jsText;
	}

	evalCode() {
		return eval(this.getCodeJs());
	}

	playCode() {
		const blockProgram = this.evalCode();
		globalScSynth.playUgenAt(blockProgram, 0, -1, 1, []);
	}

	replaceCode() {
		const blockProgram = this.evalCode();
		const replacingProgram = addWorkspaceEnvelope(blockProgram);
		globalScSynth.sendOsc(
			sc.n_set(-1, [['workspaceReleaseTime', 3], ['workspaceGate', 0]]),
		);
		globalScSynth.playUgenAt(
			replacingProgram,
			0,
			-1,
			1,
			[
				[
					'workspaceAttackTime',
					3,
				],
				[
					'workspaceReleaseTime',
					3,
				],
			],
		);
	}

	printCode() {
		return sc.prettyPrintSyndefOf(this.evalCode());
	}

	setPrettyColours() {
		this.Blockly.utils.colour.setHsvSaturation(this.colourSaturation);
		this.Blockly.utils.colour.setHsvValue(1);
		setCssProperties({
			'--toolbox-colour': '#f0fff0',
			'--paper-colour': '#fffacd',
			'--text-colour': '#424242',
			'--workspace-colour': '#fddde6',
			'--fragment-colour': '#98ece8',
			'--outline-colour': '#f6d4f6',
		});
	}

	setGreyColours() {
		this.Blockly.utils.colour.setHsvSaturation(0);
		this.Blockly.utils.colour.setHsvValue(1);
		setCssProperties({
			'--toolbox-colour': '#eee',
			'--paper-colour': '#eee',
			'--text-colour': '#000',
			'--workspace-colour': '#f3f3f3',
			'--fragment-colour': '#eee',
			'--outline-colour': '#000',
		});
	}

	loadBlockMessages(fileName) {
		return sc.fetchJson(fileName, { cache: 'no-cache' })
			.then((messages) => {
				for (const key in messages) {
					const value = messages[key];
					// console.debug(`load_block_messages: ${key} = ${value}`);
					this.Blockly.Msg[key] = value;
				}
			});
	}

	loadBlockDefinitions(fileName) {
		sc.fetchJson(fileName, { cache: 'no-cache' })
			.then(this.Blockly.defineBlocksWithJsonArray);
	}

	loadProgramOracle(fileName) {
		sc.fetchJson(fileName, { cache: 'no-cache' })
			.then((array) => this.programOracle = array);
	}

	runOracle() {
		if (this.programOracle.length > 0) {
			const which = sc.arrayChoose(this.programOracle);
			const path = `help/Program/${which}`;
			console.debug(`runOracle: ${path}`);
			this.workspace.clear();
			this.loadHelpGraph(path);
		} else {
			console.log('runOracle: Empty oracle?');
		}
	}

	loadToolbox(fileName, onCompletion) {
		sc.fetchJson(fileName, { cache: 'no-cache' })
			.then((toolbox) => this.injectWithToolbox(toolbox, onCompletion));
	}

	// Read and load .json format program from Url.
	loadProgram(jsonUrl) {
		sc.fetchUtf8(sc.urlAppendTimeStamp(jsonUrl), { cache: 'no-cache' })
			.then((jsonText) => this.loadProgramText(jsonText));
	}

	loadHelpGraph(graphPath) {
		this.loadProgram(`${graphPath}.json`, false);
		loadNotes(`${graphPath}.sp`)
			.then(sc.setterForInnerHtmlOf('blkNotes'));
		if (this.trackHistory) {
			sc.windowUrlSetParam('e', graphPath);
		}
	}

	// If the Url has fileParamKey, load the named .json/.sp files.
	maybeLoadHelpFileFromUrlParam(fileParamKey) {
		const fileName = sc.urlGetParam(fileParamKey);
		if (fileName) {
			this.loadHelpGraph(fileName);
		}
	}

	rebuildWorkspace() {
		this.loadProgramText(this.getWorkspaceJson())
	}

	nextNamingScheme() {
		this.naming = this.naming == 'Symbolic' ? 'Text' : 'Symbolic';
		this.loadBlockMessages(`json/${this.naming}Messages.json`).then(
			(unused) => this.rebuildWorkspace()
		);
	}

	nextToolbox() {
		this.whichToolbox = this.whichToolbox == 'Complete' ? 'Small' : 'Complete';
		sc.fetchJson(`json/${this.whichToolbox}Toolbox.json`, { cache: 'no-cache' })
			.then((tree) => this.workspace.updateToolbox(tree));
	}

	nextColourScheme() {
		this.whichColourScheme = this.whichColourScheme == 'Pretty'
			? 'Grey'
			: 'Pretty';
		if (this.whichColourScheme == 'Pretty') {
			this.setPrettyColours();
		} else {
			this.setGreyColours();
		}
		this.workspace.setTheme(this.Blockly.Themes.Classic);
		this.rebuildWorkspace();
	}

	playSelectedText() {
		const splText = _selectedTextOrParagraphAtCaret_1(window); /* window.getSelection().toString() */
		const jsText = sl.rewriteSlToJs(`{ ${splText} }.value.play`);
		return eval(jsText);
	}

	onWorkspaceChange() {
		// console.debug('onWorkspaceChange');
		return (event) => {
			// console.debug('onWorkspaceChange', event.type, event.element);
			if (
				event.type == Blockly.Events.BLOCK_CHANGE && event.element == 'field'
			) {
				const aBlock = this.workspace.getBlockById(event.blockId);
				if (aBlock) {
					// console.debug('onWorkspaceChange', aBlock.type);
					if (aBlock.type == 'Sc_ControlField') {
						// console.debug('onWorkspaceChange', aBlock.getFieldValue('VALUE'));
						globalScSynth.sendOsc(
							sc.n_set1(-1, aBlock.id, aBlock.getFieldValue('VALUE')),
						);
					}
				}
			}
		};
	}

	// Get .json serialization of workspace.
	getWorkspaceJson() {
		const jsonData = this.Blockly.serialization.workspaces.save(this.workspace);
		const jsonText = JSON.stringify(jsonData, null, ' ');
		return jsonText;
	}
}
